# Webbserver IPv6-only
I have started to remove A record for some of my webbservers and I will write what's not working on the server side when legacy A record and HTTPS ipv4hints is removed.
The server{s} have IPv4 connectivity.

# Wordpress Jetpak
Jetpak isn't working without A record
Wordpress API is working with autoupdates etc.
At least I got a error message now....
"There was an error reconnecting Jetpack. Error: The Jetpack server was unable to communicate with your site https://ispmedipv6.se/isp/ [IXR -32300: transport error: http_request_failed cURL error 7: Failed to connect to ispmedipv6.se port 443 after 139 ms: Could not connect to server]"

# Wordpress themes
Some Wordpress themes don't work as expected and I have not been enable to find out why some looks like shit and other is ok.

# Wordpress - wp.com
I think allmost all connection from Wordpress is broken. I can see my WP sites there but I can't do anything with them,

# Wordpress login
The login page has complete crazy design but it works.

# Wordpress plugins
Some GDPR and many other plugins is broken with IPv6 only

# Wordpress - common
Almost everything is broken... 

# hstspreload.org
You can't add a server with HSTS without A record. I tried to add A RR for [ipv6kungen.se](https://ipv6kungen.se) to make the first test to pass and then remove it. It worked but it will be removed from the list now with this error.

_Status: ipv6kungen.se was recently submitted to the preload list.
However, ipv6kungen.se has changed its behaviour since it was submitted, and will not be added to the official preload list unless the errors below are resolved:
We cannot connect to https://ipv6kungen.se using TLS ("Get \"https://ipv6kungen.se\": dial tcp [2a02:2350:5:10e:ef:8c98:9092:db4c]:443: connect: cannot assign requested address")._

# apt.fury.io

This server is used to host Debian packages for [https://containerlab.dev/](https://containerlab.dev/) and
no IPv6, therefore you can not install containerlab on an IPv6 only server.

Issue: [https://github.com/srl-labs/containerlab/issues/2030](https://github.com/srl-labs/containerlab/issues/2030)
